/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

/**
 *
 * @author usuario
 */
public class BDUsuariosProyecKeylor {

    public void InsertarEnArchivo(String lista) {
        //En esta parte voy a crear el archivo, con la ruta donde se va a guardar.
        try {
            File archivo = new File("ListaPersonas.txt");
            BufferedWriter archi = new BufferedWriter(new FileWriter(archivo, true));
            archi.write(lista + "\r\n");
            archi.close();
        } catch (Exception e) {
            System.out.println("Error al escribir en el archivo" + e);
        }
    }

    //Esto es para poder leer el archivo.
    public ArrayList<String> LeerDesdeArchivo() {
        ArrayList<String> contenido = new ArrayList<>();
        try {
            File archivo = new File("ListaPersonas.txt");
            BufferedReader archi = new BufferedReader(new FileReader(archivo));
            while (archi.ready()) {
                contenido.add(archi.readLine());
            }
            archi.close();
        } catch (Exception e) {
            System.out.println("Error al leer del archivo" + e);
        }
        return contenido;
    }

    public void guardarEnArchiComplet(ArrayList lista) {
        //En esta parte voy a crear el archivo, con la ruta donde se va a guardar.
        try {
            File archivo = new File("ListaPersonas.txt");
            BufferedWriter archi = new BufferedWriter(new FileWriter(archivo, false));
            for (Object h : lista) {
                archi.write(h + "\r\n");

            }
            archi.close();
        } catch (Exception e) {
            System.out.println("Error al escribir en el archivo" + e);
        }
    }

}
