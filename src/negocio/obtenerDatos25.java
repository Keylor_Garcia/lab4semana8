/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import datos.BDUsuariosProyecKeylor;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author usuario
 */
public class obtenerDatos25 {//Esta parte es para obtener los 25 espacios bloqueados.

    Date date = new Date();
    DateFormat hourdateFormat = new SimpleDateFormat("dd/MM/yyyy-HH:mm:ss");

    ArrayList<String> guarDatMatriz = new ArrayList<>();
    public ArrayList<String> guaradaAsientoReserv = new ArrayList<>();//En este se va a guardar los asientos reservados.
    BDUsuariosProyecKeylor llamarArchivo = new BDUsuariosProyecKeylor();

    public int precioT = 0;

    CompraBoletos obtenerRan = new CompraBoletos();

    public boolean validaCedula(String cedPersonas) {
        ArrayList<String> contenido = llamarArchivo.LeerDesdeArchivo();
        boolean validarSiEstaCed = false;
        for (Object d : contenido) {
            String[] nombre = d.toString().split(";");
            if (nombre[0].equals(cedPersonas) && nombre.length <= 3) {
                validarSiEstaCed = true;
            }

        }
        if (validarSiEstaCed == false) {
            JOptionPane.showMessageDialog(null, "La cédula no se encuentra registrada", "Error", JOptionPane.ERROR_MESSAGE);

        }
        return validarSiEstaCed;

    }

    public void guardDatos(String cedPersonas) {
        ArrayList<String> contenido = llamarArchivo.LeerDesdeArchivo();
        String actualizarInform = "";

        for (int i = 0; i < contenido.size(); i++) {
            String nombre[] = contenido.get(i).split(";");
            if (nombre[0].equals(cedPersonas)) {
                actualizarInform = contenido.get(i) + ";" + String.valueOf(precioT) + ";" + guaradaAsientoReserv.toString()+";" +hourdateFormat.format(date);
                contenido.remove(contenido.get(i));
            }

        }

        contenido.add(actualizarInform);
        llamarArchivo.guardarEnArchiComplet(contenido);

    }

    public void calcularPrecio(String precio) {

        String j[] = precio.split("");
        if (Integer.parseInt(j[0]) >= 5) {
            precioT += 3000;

        } else {

            precioT += 5000;

        }

    }

    public void espaciosBloqueados() {
        Random randon = new Random();

        String[][] matriz = obtenerRan.espacios;

        for (int i = 0; i < 25; i++) {

            int guardRan = randon.nextInt(matriz.length);
            int guardRan2 = randon.nextInt(matriz[0].length);
            boolean espacioExist = true;//Validacion
            for (Object z : guarDatMatriz) {
                if (matriz[guardRan][guardRan2].equals(z)) {
                    espacioExist = false;

                }

            }

            if (espacioExist == true) {
                guarDatMatriz.add(matriz[guardRan][guardRan2]);
                matriz[guardRan][guardRan2] = "XX";

            }

        }

    }

    public String imprimir() {

        String[][] matri = obtenerRan.espacios;
        String datMatriz = "";

        for (int i = 0; i < matri.length; i++) {
            for (int c = 0; c < matri[i].length; c++) {

                datMatriz += matri[i][c].toString() + " ";

            }
            datMatriz += "\n";

        }
        return datMatriz;

    }

    public void asientosReservados(String asiento) {

        boolean asientoExit = true;
        for (Object d : guarDatMatriz) {
            if (d.equals(asiento)) {
                asientoExit = false;

            }

        }
        if (asientoExit == true && guaradaAsientoReserv.size() <= 5) {
            guarDatMatriz.add(asiento);
            guaradaAsientoReserv.add(asiento);
            String[][] matri = obtenerRan.espacios;
            calcularPrecio(asiento);

            for (int i = 0; i < matri.length; i++) {

                for (int c = 0; c < matri[i].length; c++) {

                    if (matri[i][c].equals(asiento)) {
                        matri[i][c] = "XX";

                    }

                }

            }

        }

    }

}
